import Phaser from "phaser";

export default class MultiPipelineTest extends Phaser.Renderer.WebGL.Pipelines.MultiPipeline {
  private _key: string;

  get key(): string {
    return this._key;
  }

  constructor(game: Phaser.Game, fragShader: string, key: string) {
    let config = {
      game: game,
      renderer: game.renderer,
      fragShader: fragShader,
    };

    super(config);
    this._key = key;
  }
}

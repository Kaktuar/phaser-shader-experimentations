import Phaser from "phaser";

export default class SinglePipelineTest extends Phaser.Renderer.WebGL.Pipelines.SinglePipeline {
  private _key: string;

  get key(): string {
    return this._key;
  }

  constructor(game: Phaser.Game, fragShader: string, key: string) {
    let config = {
      game: game,
      renderer: game.renderer,
      fragShader: fragShader,
    };

    super(config);
    this._key = key;
  }
}

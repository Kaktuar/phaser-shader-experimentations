export default class BitmapTextureHandler {
  bitmap: Phaser.Textures.Texture;
  bitmapCanvas!: HTMLCanvasElement;
  bitmapCtx!: CanvasRenderingContext2D;
  quant: number;
  canvas: HTMLCanvasElement;

  constructor(texture: Phaser.Textures.Texture, quant: number, canvas: HTMLCanvasElement) {
    this.bitmap = texture;
    this.quant = quant;
    this.canvas = canvas;
    this.init();
  }

  private init() {
    const bitmapImage = this.bitmap.getSourceImage();
    this.bitmapCanvas = document.createElement('canvas');
    this.bitmapCanvas.width = this.canvas.width;
    this.bitmapCanvas.height = this.canvas.height;
    this.bitmapCtx = this.bitmapCanvas.getContext('2d')!;
    this.bitmapCtx.drawImage(bitmapImage as CanvasImageSource, 0, 0, this.canvas.width, this.canvas.height);
  }

  getPixelColor(x: number, y: number): Phaser.Display.Color {
    const pixelData = this.bitmapCtx.getImageData(x, y, 1, 1).data;
    return new Phaser.Display.Color(pixelData[0], pixelData[1], pixelData[2], pixelData[3]);
  }

  getPixelShaderColor(x: number, y: number): [number, number, number] {
    const pixelData = this.bitmapCtx.getImageData(x, y, 1, 1).data;
    return [pixelData[0] / 255.0, pixelData[1] / 255.0, pixelData[2] / 255.0];
  }

  getQuantizedValue(x: number, y: number): number {
    const color = this.getPixelColor(x, y);

    // TODO : better quantization, this is a temporary hash function
    let quantizedValue = Math.floor(color.green / (255 / this.quant));
    if (color.blue > 250) {
      quantizedValue = 0;
    }
    return quantizedValue;
  }

  static getShaderColorFromHexString(hexStringColor: string): number[] {
    const color = Phaser.Display.Color.HexStringToColor(hexStringColor);
    return [color.red / 255.0, color.green / 255.0, color.blue / 255.0];
  }
}

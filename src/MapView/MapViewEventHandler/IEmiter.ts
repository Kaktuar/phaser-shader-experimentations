// Using the same interface as 4X EventEmmiter to be able to quickly switch after
export default interface IEmitter {
  on: (event: string, callback: (data: any) => void) => void;
  emit: (event: string, data: any) => void;
  remove: (event: string, callback: (data: any) => void) => void;
}

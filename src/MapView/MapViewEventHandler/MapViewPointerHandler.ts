import { MapViewData, ProvinceViewData, RegionViewData } from '../MapViewData';
import BitmapTextureHandler from './BitmapTextureHandler';
import type IEmitter from './IEmiter';
import TempEventEmitter from './TempEventEmmiter';

export default class MapViewPointerHandler {
  bitmapTextureHandler: BitmapTextureHandler;
  private mapViewData: MapViewData;
  private emitter: IEmitter;

  onClick(f: (pos: { x: number; y: number }) => void) {
    this.emitter.on('click', f);
  }

  onRegionClick(f: (regionId: number) => void) {
    this.emitter.on('regionClick', f);
  }

  onProvinceClick(f: (provinceId: number) => void) {
    this.emitter.on('provinceClick', f);
  }

  onPointerMove(f: (data: MapPointerData) => void) {
    this.emitter.on('move', f);
  }

  constructor(
    bitmap: Phaser.Textures.Texture,
    scene: Phaser.Scene,
    mapViewData: MapViewData,
    mapSprite: Phaser.GameObjects.Sprite,
    quant: number,
  ) {
    this.bitmapTextureHandler = new BitmapTextureHandler(bitmap, quant, scene.game.canvas);
    this.mapViewData = mapViewData;
    this.emitter = new TempEventEmitter(); // TODO : BETTER INJECTION, USE RIGHT EVENTEMITTER
    this.initializeClickEvents(mapSprite, scene);
    this.initializeHoverEvents(mapSprite, scene);
  }

  private initializeClickEvents(mapSprite: Phaser.GameObjects.Sprite, scene: Phaser.Scene) {
    mapSprite.setInteractive();
    mapSprite.on('pointerdown', () => {
      const pos = scene.input.activePointer.position;
      const quantizedValue = this.bitmapTextureHandler.getQuantizedValue(pos.x, pos.y);
      const x = Math.floor(pos.x * 10000) / 10000;
      const y = Math.floor(pos.y * 10000) / 10000;
      const province = this.mapViewData.provinces[quantizedValue];
      const region = this.mapViewData.getProvinceRegion(province);
      this.emitter.emit('provinceClick', province.id);
      if (region) this.emitter.emit('regionClick', region.id);
      this.emitter.emit('click', { x, y });
    });
  }

  private initializeHoverEvents(mapSprite: Phaser.GameObjects.Sprite, scene: Phaser.Scene) {
    mapSprite.on('pointermove', () => {
      const pos = scene.input.activePointer.position;
      const quantizedValue = this.bitmapTextureHandler.getQuantizedValue(pos.x, pos.y);
      const province = this.mapViewData.getProvinceByQuantization(quantizedValue);
      const region = province ? this.mapViewData.getProvinceRegion(province) : undefined;
      if (province) this.emitter.emit('move', { position: pos, province, region, quantization: quantizedValue });
    });
  }
}

export interface MapPointerData {
  position: { x: number; y: number };
  province?: ProvinceViewData;
  region?: RegionViewData;
  quantization: number;
}

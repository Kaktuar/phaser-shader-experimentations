import IEmitter from './IEmiter';

export default class TempEventEmitter implements IEmitter {
  private listeners: { [key: string]: ((data: any) => void)[] } = {};

  on(event: string, callback: (data: any) => void): void {
    if (!this.listeners[event]) {
      this.listeners[event] = [];
    }
    this.listeners[event].push(callback);
  }

  emit(event: string, data: any): void {
    const eventListeners = this.listeners[event];
    if (eventListeners) {
      eventListeners.forEach((callback) => callback(data));
    }
  }

  remove(event: string, callback: (data: any) => void): void {
    const eventListeners = this.listeners[event];
    if (eventListeners) {
      this.listeners[event] = eventListeners.filter((listener) => listener !== callback);
    }
  }
}

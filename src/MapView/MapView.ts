import { MapViewCategory } from './MapViewTypes';
import MapViewPointerHandler from './MapViewEventHandler/MapViewPointerHandler';
import type { MapViewSprites } from '.';
import { MapViewData } from './MapViewData';
import type { IMapDataFetcher, IMapViewItem } from './MapViewData';
import MapViewRenderer from './MapViewRenderer/MapViewRenderer';

export default class MapView {
  mapViewData: MapViewData;
  pointerHandler!: MapViewPointerHandler;
  mapViewType: MapViewCategory = MapViewCategory.Provinces;
  private renderer: MapViewRenderer;
  private scene: Phaser.Scene;
  private initialized = false;

  // Move
  private cursorText!: Phaser.GameObjects.Text;
  private displayNames = true;

  constructor(scene: Phaser.Scene, mapViewSprites: MapViewSprites, mapDataFetcher: IMapDataFetcher) {
    this.cursorInitialization(scene);
    this.scene = scene;
    this.mapViewData = new MapViewData(mapDataFetcher);
    this.preload(mapViewSprites);
    this.renderer = new MapViewRenderer(scene);
  }

  private preload(sprites: MapViewSprites) {
    if (!this.scene.textures.exists(sprites.map.key)) this.scene.load.image(sprites.map.key, sprites.map.url);
    if (!this.scene.textures.exists(sprites.bitmap.key)) this.scene.load.image(sprites.bitmap.key, sprites.bitmap.url);
    if (!this.scene.textures.exists(sprites.mapBorders.key)) {
      this.scene.load.image(sprites.mapBorders.key, sprites.mapBorders.url);
    }
    if (!this.scene.textures.exists(sprites.mapGlow.key)) {
      this.scene.load.image(sprites.mapGlow.key, sprites.mapGlow.url);
    }
  }

  async create() {
    await this.mapViewData.initialize();
    const mapSprite = this.scene.add.sprite(0, 0, 'Map');

    const provinceColors = this.mapViewData.provinces.map((p) => [p.id, ...p.getShaderColor]);

    // Initialize all pipelines
    this.renderer.setupMapViewPipelines(provinceColors, mapSprite);

    // Initialize MapViewMouseHandler
    const bitmap = this.scene.textures.get('Bitmap');
    this.pointerHandler = new MapViewPointerHandler(bitmap, this.scene, this.mapViewData, mapSprite, 13);
    this.pointerHandler.onPointerMove(this.onPointerMove.bind(this));
    this.initialized = true;
  }

  update() {
    if (!this.initialized) return;

    this.renderer.pipeline.update({
      uUseRegion: this.mapViewType === MapViewCategory.Regions,
      uTime: this.scene.time.now / 100,
      uUseDebug: false,
      uDebugColor: [1, 0, 1],
    });
  }

  private onPointerMove(data: any) {
    const { province } = data;
    const { region } = data;
    const { x } = data.position;
    const { y } = data.position;
    this.updateCursorText(data.province as IMapViewItem, new Phaser.Math.Vector2(data.position.x, data.position.y));

    this.renderer.pipeline.update({
      uCurrentProvinceColor: province!.getShaderColor,
      uCurrentQuantization: data.quantization,
      uMousePos: [x, y],
      uCursorColor: this.pointerHandler.bitmapTextureHandler.getPixelShaderColor(x, y),
      uCurrentRegionColor: region?.getShaderColor,
      uProvinceCount: region?.provinceIds.length,
      uProvinces: region?.provinceIds,
      uGlowColor: province!.getShaderColor,
    });
  }

  toggleDisplayNames(enabled: boolean) {
    this.displayNames = enabled;
    if (!enabled) this.cursorText.setText('');
  }

  changeRendererPipeline() {
    this.renderer.switchPipeline();

    // TODO : remove, this is just a hack to update the pipeline uniforms on pipeline change
    const pos = this.scene.input.activePointer.position;
    const quantizedValue = this.pointerHandler.bitmapTextureHandler.getQuantizedValue(pos.x, pos.y);
    const province = this.mapViewData.getProvinceByQuantization(quantizedValue);
    const region = province ? this.mapViewData.getProvinceRegion(province) : undefined;
    this.onPointerMove({
      position: pos,
      province,
      region,
      quantization: quantizedValue,
    });
  }

  private cursorInitialization(scene: Phaser.Scene) {
    this.cursorText = scene.add.text(0, 0, '', { fontSize: '50px', color: '#ffffff' });
    this.cursorText.setOrigin(0, 0);
    this.cursorText.setDepth(1);
  }

  private updateCursorText(mapViewItem: IMapViewItem, mousePosition: Phaser.Math.Vector2) {
    if (!this.displayNames) return;

    this.cursorText.setText(mapViewItem.name);
    if (
      !mapViewItem.useAbsolutePosition ||
      (mapViewItem.absolutePosition.x === 0 && mapViewItem.absolutePosition.y === 0)
    ) {
      this.cursorText.setPosition(mousePosition.x - 80, mousePosition.y - 30);
    } else this.cursorText.setPosition(mapViewItem.absolutePosition.x - 80, mapViewItem.absolutePosition.y - 30);
  }

  refreshRendering() {
    this.renderer.refresh();
  }
}

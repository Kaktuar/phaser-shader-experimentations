export function relativeTo(
  x: number,
  y: number,
  object: Phaser.GameObjects.Components.Transform,
  camera: Phaser.Cameras.Scene2D.Camera,
) {
  const objectPosition = {
    x: object.x - camera.worldView.x,
    y: object.y - camera.worldView.y,
  };

  return {
    x: x / camera.zoom - objectPosition.x,
    y: y / camera.zoom - objectPosition.y,
  };
}

export function getRelativePositionToCanvas(
  gameObject: Phaser.GameObjects.Components.Transform,
  camera: Phaser.Cameras.Scene2D.Camera,
) {
  return {
    x: (gameObject.x - camera.worldView.x) * camera.zoom,
    y: (gameObject.y - camera.worldView.y) * camera.zoom,
  };
}

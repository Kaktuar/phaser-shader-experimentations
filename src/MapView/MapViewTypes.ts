export interface MapViewSprites {
  map: SpriteAssociation;
  bitmap: SpriteAssociation;
  mapBorders: SpriteAssociation;
  mapGlow: SpriteAssociation;
}

interface SpriteAssociation {
  key: string;
  url: string;
}

export enum MapViewCategory {
  Provinces,
  Regions,
}

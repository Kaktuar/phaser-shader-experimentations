import Phaser from 'phaser';
import type { MainSampler, MapUniformData, MapUniformDataBase } from '..';

export default class MapViewPipeline extends Phaser.Renderer.WebGL.Pipelines.MultiPipeline {
  private pipelineKey: string;
  private uniform: MapUniformData = {};
  private mainSample: MainSampler;

  get key(): string {
    return this.pipelineKey;
  }

  constructor(game: Phaser.Game, fragShader: string, key: string) {
    const config = {
      game,
      renderer: game.renderer,
      fragShader,
    };

    super(config);
    this.pipelineKey = key;
  }

  initialize(uniform: MapUniformDataBase) {
    this.uniform = uniform;
    this.mainSample = uniform.uMainSampler;
    this.set2fv('uResolution', uniform.uResolution);
    this.refreshTextureBindings();
    this.set4fv('uProvinceColors', uniform.uProvinceColors);
  }

  refreshTextureBindings() {
    for (let i = 0; i < this.mainSample.length; i++) {
      this.setTexture2D(this.mainSample[i]);
      this.bindTexture(this.mainSample[i], i);
    }
  }

  update(uniform: MapUniformData) {
    if (uniform.uResolution !== undefined) {
      this.uniform.uResolution = uniform.uResolution;
      this.set2fv('uResolution', uniform.uResolution);
    }
    if (uniform.uCurrentProvinceColor !== undefined) {
      this.uniform.uCurrentProvinceColor = uniform.uCurrentProvinceColor;
      this.set3fv('uCurrentProvinceColor', uniform.uCurrentProvinceColor);
    }
    if (uniform.uCurrentQuantization !== undefined) {
      this.uniform.uCurrentQuantization = uniform.uCurrentQuantization;
      this.set1i('uCurrentQuantization', uniform.uCurrentQuantization);
    }
    if (uniform.uUseRegion !== undefined) {
      this.uniform.uUseRegion = uniform.uUseRegion;
      this.set1i('uUseRegion', uniform.uUseRegion ? 1 : 0);
    }
    if (uniform.uCurrentRegionColor !== undefined) {
      this.uniform.uCurrentRegionColor = uniform.uCurrentRegionColor;
      this.set3fv('uCurrentRegionColor', uniform.uCurrentRegionColor);
    }
    if (uniform.uMousePos !== undefined) {
      this.uniform.uMousePos = uniform.uMousePos;
      this.set2fv('uMousePos', uniform.uMousePos);
    }
    if (uniform.uCursorColor !== undefined) {
      this.uniform.uCursorColor = uniform.uCursorColor;
      this.set3fv('uCursorColor', uniform.uCursorColor);
    }
    if (uniform.uProvinceCount !== undefined) {
      this.uniform.uProvinceCount = uniform.uProvinceCount;
      this.set1i('uProvinceCount', uniform.uProvinceCount);
    }
    if (uniform.uProvinces !== undefined) {
      this.uniform.uProvinces = uniform.uProvinces;
      this.set1iv('uProvinces', uniform.uProvinces);
    }
    if (uniform.uGlowColor !== undefined) {
      this.uniform.uGlowColor = uniform.uGlowColor;
      this.set3fv('uGlowColor', uniform.uGlowColor);
    }
    if (uniform.uTime !== undefined) {
      this.uniform.uTime = uniform.uTime;
      this.set1f('uTime', uniform.uTime);
    }
    if (uniform.uUseDebug !== undefined) {
      this.uniform.uUseDebug = uniform.uUseDebug;
      this.set1i('uUseDebug', uniform.uUseDebug ? 1 : 0);
    }
    if (uniform.uDebugIndex !== undefined) {
      this.uniform.uDebugIndex = uniform.uDebugIndex;
      this.set1i('uDebugIndex', uniform.uDebugIndex);
    }
    if (uniform.uDebugColor !== undefined) {
      this.uniform.uDebugColor = uniform.uDebugColor;
      this.set3fv('uDebugColor', uniform.uDebugColor);
    }
  }
}

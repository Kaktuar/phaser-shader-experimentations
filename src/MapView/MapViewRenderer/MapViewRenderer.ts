import MapViewPipeline from './MapViewPipeline';
import { MainSampler } from './Uniforms';

export default class MapViewRenderer {
  private scene: Phaser.Scene;
  private mapSprite!: Phaser.GameObjects.Sprite;
  // TEMP
  mapViewPipelineKeys: string[] = [
    'MapViewPipelineTest',
    'MapViewSimple',
    'MapNoGlow',
    'MapNoBackground',
    'MapRegion',
    'MapViewDebug',
    'MapViewGlowDebug',
  ];
  pipelines: MapViewPipeline[] = [];
  currentPipeline = 0;

  constructor(scene: Phaser.Scene) {
    this.scene = scene;
  }
  get pipeline() {
    return this.pipelines[this.currentPipeline];
  }

  setupMapViewPipelines(uProvinceColors: number[][], mapSprite: Phaser.GameObjects.Sprite) {
    const map = mapSprite.texture.source[0].glTexture as WebGLTexture;
    const backgroundMapWebHLTexture = this.scene.textures.get('Bitmap').source[0].glTexture as WebGLTexture;
    const mapBorders = this.scene.textures.get('MapBorders').source[0].glTexture as WebGLTexture;
    const mapGlow = this.scene.textures.get('MapGlow').source[0].glTexture as WebGLTexture;
    const textures = [map, backgroundMapWebHLTexture, mapBorders, mapGlow] as MainSampler;

    this.mapViewPipelineKeys.forEach((key) => {
      const pipeline = (this.scene.game.renderer as Phaser.Renderer.WebGL.WebGLRenderer).pipelines.get(
        key,
      ) as MapViewPipeline;
      this.pipelines.push(pipeline);
      pipeline.initialize({
        uResolution: [this.scene.scale.width, this.scene.scale.height],
        uMainSampler: textures,
        uProvinceColors: uProvinceColors.flat(),
      });
    });

    this.mapSprite = mapSprite;
    this.mapSprite.setOrigin(0, 0);
    this.mapSprite.setDisplaySize(this.scene.scale.width, this.scene.scale.width);
    this.mapSprite.setPipeline(this.pipeline);
  }

  // To call if we switch scene
  refresh() {
    this.pipeline.refreshTextureBindings();
  }

  // TODO Improve
  switchPipeline() {
    this.currentPipeline = (this.currentPipeline + 1) % this.mapViewPipelineKeys.length;
    this.mapSprite.setPipeline(this.pipeline);
    this.refresh(); // Not needed, it's just for debug purpose -> TODO remove
  }
}

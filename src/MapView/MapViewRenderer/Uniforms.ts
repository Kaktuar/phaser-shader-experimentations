export type MainSampler = [WebGLTexture, ...WebGLTexture[]];

export interface MapUniformDataBase {
  uResolution: [number, number];
  uMainSampler: MainSampler;
  uProvinceColors: number[];
  uCurrentProvinceColor?: [number, number, number];
  uCurrentQuantization?: number;
  uUseRegion?: boolean;
  uCurrentRegionColor?: [number, number, number];
  uMousePos?: [number, number];
  uCursorColor?: [number, number, number];
  uProvinceCount?: number;
  uProvinces?: number[];
  uGlowColor?: [number, number, number];
  uTime?: number;
  uUseDebug?: boolean;
  uDebugIndex?: number;
  uDebugColor?: [number, number, number];
}

export type MapUniformData = Partial<MapUniformDataBase>;

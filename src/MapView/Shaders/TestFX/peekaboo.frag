precision mediump float;

uniform vec2 resolution;
uniform sampler2D u_texture_1;
uniform vec2 mousePos;

void main(){
	vec2 uv=gl_FragCoord.xy/resolution.xy;
	uv.y=1.-uv.y;
	vec4 texColor=texture2D(u_texture_1,uv);
	vec4 cursorColor=texture2D(u_texture_1,mousePos/resolution.xy);
	float d=smoothstep(0.,.4,distance(uv,mousePos/resolution.xy));
	gl_FragColor=texColor-d;
}
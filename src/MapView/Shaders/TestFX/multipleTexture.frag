precision mediump float;

uniform vec2 resolution;
uniform sampler2D uMainSampler[2];

void main(){
	vec2 uv=gl_FragCoord.xy/resolution.xy;
	vec4 texColor=texture2D(uMainSampler[1],uv);
	gl_FragColor=texColor;
}
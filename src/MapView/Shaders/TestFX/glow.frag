precision mediump float;

// Initialization only
uniform vec2 u_resolution;
uniform sampler2D uMainSampler;

// Dynamic parameters
uniform vec3 u_glowColor;
uniform float u_time;// set to 0.0 at initialization for no animation

void main(){
    vec2 uv=gl_FragCoord.xy/u_resolution.xy;
    uv.y=1.-uv.y;
    vec4 color=texture2D(uMainSampler,uv);
    
    float radius=(.015*sin(u_time/3.)+.035)*1.5;// TODO better easing
    
    vec4 sum=vec4(0.);
    vec2 offset=vec2(0.,0.);
    
    const int sample=16;
    for(int i=0;i<sample;i++){
        float angle=360.*float(i)/float(sample);
        
        offset.x=radius*cos(radians(angle));
        offset.y=radius*sin(radians(angle));
        
        sum+=texture2D(uMainSampler,offset+uv);
        sum+=texture2D(uMainSampler,offset*.8+uv);
        sum+=texture2D(uMainSampler,offset*.6+uv);
        sum+=texture2D(uMainSampler,offset*.4+uv);
        sum+=texture2D(uMainSampler,offset*.2+uv);
    }
    sum/=float(sample*5);
    
    gl_FragColor= sum-color;
    // vec4 glow=vec4(u_glowColor,sum.a);
    // glow.rgb*=glow.a;// Pre-multiply RGB channels by alpha
    // vec4 glowf=mix(vec4(0.,0.,0.,0.),glow,1.-color.a);
    // gl_FragColor=glowf;
}
// Shader map with internal handling of color quantization
precision mediump float;

uniform vec2 uResolution;
uniform sampler2D uMainSampler[2];
uniform vec2 uMousePos;

float quantizedPinkScale(vec4 color,float numLevels){
    return floor(color.g*numLevels);
}

vec4 getColor(int index)
{
    vec3 colors[13];
    colors[0]=vec3(1.,0.,0.);
    colors[1]=vec3(1.,.5,0.);
    colors[2]=vec3(1.,1.,0.);
    colors[3]=vec3(.5,1.,0.);
    colors[4]=vec3(0.,1.,0.);
    colors[5]=vec3(0.,1.,.5);
    colors[6]=vec3(0.,1.,1.);
    colors[7]=vec3(0.,.5,1.);
    colors[8]=vec3(0.,0.,1.);
    colors[9]=vec3(.5,0.,1.);
    colors[10]=vec3(1.,0.,1.);
    colors[11]=vec3(1.,0.,.5);
    colors[12]=vec3(1.,1.,1.);
    
    for(int i=0;i<13;++i){
        if(i==index){
            return vec4(colors[i].rgb,1.);
        }
    }
    
    return vec4(colors[0].rgb,1.);
}

void main(){
    vec2 uv=gl_FragCoord.xy/uResolution.xy;
    uv.y=1.-uv.y;
    vec4 map=texture2D(uMainSampler[0],uv);
    vec4 colorMap=texture2D(uMainSampler[1],uv);
    vec4 cursorColor=texture2D(uMainSampler[1],uMousePos/uResolution.xy);
    
    float cursorQuant=quantizedPinkScale(cursorColor,13.)/13.;
    vec4 currentColor=getColor(int(cursorQuant*13.));
    float d=smoothstep(0.,.5,distance(uv,uMousePos/uResolution.xy));
    
    float fragmentQuant=quantizedPinkScale(colorMap,13.)/13.;
    if(cursorQuant==fragmentQuant){
        gl_FragColor=vec4(1.,1.,1.,1.);
    }
    else{
        gl_FragColor=currentColor;
    }
}
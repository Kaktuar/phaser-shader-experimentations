// Shader map with internal handling of color quantization
precision mediump float;

uniform vec2 uResolution;
uniform sampler2D uMainSampler[4];
uniform vec3 uCurrentProvinceColor;
uniform int uCurrentQuantization;
uniform vec2 uMousePos;
uniform vec3 uCursorColor;

// Region handling -> TODO other shader
uniform bool uUseRegion;
uniform int uProvinceCount;
uniform int uProvinces[256];
uniform vec4 uProvinceColors[13];
uniform vec3 uCurrentRegionColor;

// Glow
uniform vec3 uGlowColor;
uniform float uTime;// set to 0.0 at initialization for no animation
float glowRadius=.0312;
float glowPulsationRadius=.0072;
float glowPulsationPeriod=5.;

float quantizedColorLevel(vec4 color,float numLevels){
    if(color.b>250./255.)
    return 0.;
    return floor(color.g*numLevels);
}

// TODO better easing
float easing(float t){
    return(sin(t)+1.)/2.;
}

void main(){
    vec2 uv=gl_FragCoord.xy/uResolution.xy;
    uv.y=1.-uv.y;
    
    vec4 map=texture2D(uMainSampler[0],uv);
    vec4 colorMap=texture2D(uMainSampler[1],uv);
    vec4 colorGlowMap=texture2D(uMainSampler[3],uv);
    vec4 colorBorders=texture2D(uMainSampler[2],uv);
    
    vec4 cursorColor=vec4(uCursorColor,1.);
    float cursorQuant=quantizedColorLevel(cursorColor,13.)/13.;
    int cursorQuantInt=int(quantizedColorLevel(cursorColor,13.));
    
    float fragmentQuant=quantizedColorLevel(colorMap,13.)/13.;
    int fragmentQuantInt=int(quantizedColorLevel(colorMap,13.));
    vec4 currentColor=vec4(0.);
    vec4 currentGlowColor=vec4(0.);
    float fragmentGlowQuant=quantizedColorLevel(colorGlowMap,13.)/13.;
    
    vec4 glow=vec4(0.);
    
    if(uUseRegion){
        bool foundMatch=false;
        for(int i=0;i<256;i++){
            if(i>uProvinceCount-1)break;
            if(uProvinces[i]==fragmentQuantInt){
                foundMatch=true;
                break;
            }
        }
        if(foundMatch){
            currentColor=vec4(uCurrentRegionColor,1.);
            gl_FragColor=currentColor;
        }
        else{
            gl_FragColor=map;
        }
    }
    else{
        vec4 provinceColor;
        if(cursorQuant==fragmentQuant){
            currentColor=vec4(uCurrentProvinceColor,1.);
            // currentColor=vec4(uCurrentProvinceColor+0.05*easing(uTime/5.),1.);
            provinceColor=currentColor;
        }
        else{
            //provinceColor=map;
            //TODO : refactor
            int index=fragmentQuantInt;
            for(int i=0;i<13;i++){
                if(int(uProvinceColors[i][0])==index){
                    provinceColor=mix(vec4(uProvinceColors[i][1],uProvinceColors[i][2],uProvinceColors[i][3],1.),map,1.);
                    break;
                }
            }
        }
        
        // GLOWING
        if(fragmentGlowQuant==cursorQuant){
            currentGlowColor=vec4(uGlowColor,1.);
        }
        
        if(cursorQuant!=0.){
            float radius=glowPulsationRadius*easing(uTime/glowPulsationPeriod)+glowRadius;
            
            vec4 sum=vec4(0.,0.,0.,0.);
            vec2 offset=vec2(0.,0.);
            
            const int sample=32;
            
            for(int i=0;i<sample;i++){
                float angle=360.*float(i)/float(sample);
                
                offset.x=radius*cos(radians(angle));
                offset.y=radius*sin(radians(angle));
                
                vec4 sampleColor;
                float threshold=.003;
                sampleColor=texture2D(uMainSampler[3],clamp(offset+uv,vec2(0.+threshold),vec2(1.-threshold)));
                if(cursorQuant==quantizedColorLevel(sampleColor,13.)/13.)sum+=sampleColor;
                sampleColor=texture2D(uMainSampler[3],clamp(offset*.9+uv,vec2(0.+threshold),vec2(1.-threshold)));
                if(cursorQuant==quantizedColorLevel(sampleColor,13.)/13.)sum+=sampleColor;
                sampleColor=texture2D(uMainSampler[3],clamp(offset*.8+uv,vec2(0.+threshold),vec2(1.-threshold)));
                if(cursorQuant==quantizedColorLevel(sampleColor,13.)/13.)sum+=sampleColor;
            }
            sum/=float(sample*4);// should divide by 3 but 4 looks better
            
            vec4 glowTemp=vec4(uGlowColor,sum.a);
            glowTemp.rgb*=glowTemp.a;// Pre-multiply RGB channels by alpha
            
            glow=mix(vec4(0.,0.,0.,0.),glowTemp,1.-currentGlowColor.a);
        }
        
        gl_FragColor=provinceColor;
    }
    
    // ALPHA Blending
    gl_FragColor.rgb=mix(gl_FragColor.rgb,colorBorders.rgb,colorBorders.a);
    
    // Add glow
    gl_FragColor.rgb+=glow.rgb;
    
    // Cursor light
    float d=smoothstep(0.,.4,distance(uv,uMousePos/uResolution.xy));
    gl_FragColor+=(1.-d)*vec4(uCurrentProvinceColor.rgb,1.)*.15;
}

// TODO fix glow artifacts :
//  - Further investigation + use smaller assets for faster iteration
//  - Mask approach -> bigger glowColor to use it as a mask
//  - Create a mask that is 1.0 when the fragment color matches the "currentColor"

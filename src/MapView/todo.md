- Should update texture binding on each scene switch
- Move text handling from MapView to MapViewwRenderer
- FIX : fixed size uProvinceColors array "uniform vec4 uProvinceColors[13];"
- Clean MapView.create
- Glowing effet for regions
- Handling province & region selection
- Debug options for quick shader tweak
- REFACTOR : put elsewhere MapView debug options
- FIX : Glowing effect artifact
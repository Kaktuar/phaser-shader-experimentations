import type IMapViewItem from './IMapViewItem';
import ProvinceViewData from './ProvinceViewData';

export default class RegionViewData implements IMapViewItem {
  id: number;
  name: string;
  color: string;
  provinces!: ProvinceViewData[];
  provinceIds: number[];
  absolutePosition: { x: number; y: number };
  useAbsolutePosition = false;

  constructor({
    id,
    name,
    color,
    provinceIds,
    absolutePosition,
    useAbsolutePosition,
  }: {
    id: number;
    name: string;
    color: string;
    provinceIds: number[];
    absolutePosition?: { x: number; y: number };
    useAbsolutePosition?: boolean;
  }) {
    this.id = id;
    this.name = name;
    this.color = color;
    this.provinceIds = provinceIds;
    this.absolutePosition = absolutePosition ?? { x: 0, y: 0 };
    this.useAbsolutePosition = useAbsolutePosition ?? true;
  }

  get getShaderColor(): [number, number, number] {
    const color = Phaser.Display.Color.HexStringToColor(this.color);
    return [color.red / 255.0, color.green / 255.0, color.blue / 255.0];
  }
}

import type { IMapDataFetcher } from '..';
import ProvinceViewData from '../ProvinceViewData';
import RegionViewData from '../RegionViewData';

async function loadJson(filePath: string) {
  try {
    const module = await import(filePath);
    return module.default;
  } catch (error) {
    console.error(`Error loading ${filePath}:, ${error}`);
    return [];
  }
}

export default class MockMapDataFetcher implements IMapDataFetcher {
  private readonly provincesJsonPath = './MockData/provinces.json';
  private readonly regionsJsonPath = './MockData/regions.json';

  fetchProvincesData(): Promise<ProvinceViewData[]> {
    return loadJson(this.provincesJsonPath);
  }
  fetchRegionsData(): Promise<RegionViewData[]> {
    return loadJson(this.regionsJsonPath);
  }
}

import ProvinceViewData from '../ProvinceViewData';
import RegionViewData from '../RegionViewData';

export default interface IMapDataFetcher {
  fetchProvincesData(): Promise<ProvinceViewData[]>;
  fetchRegionsData(): Promise<RegionViewData[]>;
}

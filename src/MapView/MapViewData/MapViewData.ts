import ProvinceViewData from './ProvinceViewData';
import RegionViewData from './RegionViewData';
import type IMapDataFetcher from './MapViewDataFetcher/IMapDataFetcher';

export default class MapViewData {
  private regionsData!: RegionViewData[];
  private provincesData!: ProvinceViewData[];
  private mapDataFetcher: IMapDataFetcher;

  constructor(mapDataFetcher: IMapDataFetcher) {
    this.mapDataFetcher = mapDataFetcher;
  }

  get regions() {
    return this.regionsData;
  }

  get provinces() {
    return this.provincesData;
  }

  getRegion(id: number): RegionViewData | null {
    return this.regionsData.find((region) => region.id === id) ?? null;
  }

  getProvince(id: number): ProvinceViewData | null {
    return this.provincesData.find((province) => province.id === id) ?? null;
  }

  getProvinceByQuantization(quant: number): ProvinceViewData | null {
    return this.provincesData.find((province) => province.hash === quant) ?? null;
  }

  getProvinceRegion(province: ProvinceViewData): RegionViewData | null {
    // TODO : improve
    if (province.id === 0) return null;

    const region = this.regionsData.find((regionData) => regionData.provinceIds.includes(province.id));

    if (region === undefined) throw new Error(`Region with province id ${province.id} not found`);

    return region;
  }

  async initialize() {
    await this.initializeProvinces();
    await this.initializeRegions();
  }

  async initializeProvinces() {
    const provincesJson = await this.mapDataFetcher.fetchProvincesData();
    this.provincesData = provincesJson.map(
      (province: ProvinceViewData) =>
        new ProvinceViewData({
          id: province.id,
          name: province.name,
          color: province.color,
          hash: province.hash,
          welthLevel: province.welthLevel,
          absolutePosition: province.absolutePosition,
          useAbsolutePosition: true,
        }),
    );
  }

  async initializeRegions() {
    const regionsJson = await this.mapDataFetcher.fetchRegionsData();
    // TODO improve
    try {
      this.regionsData = regionsJson.map((region: RegionViewData) => {
        const regionViewData = new RegionViewData({
          id: region.id,
          name: region.name,
          color: region.color,
          provinceIds: region.provinceIds,
          absolutePosition: region.absolutePosition,
          useAbsolutePosition: false,
        });
        // TODO : dirty -> improve
        regionViewData.provinces = this.provincesData.filter((province) => region.provinceIds.includes(province.id));
        return regionViewData;
      });
    } catch (e) {
      console.log(e);
    }
  }
}

export default interface IMapViewItem {
  name: string;
  absolutePosition: { x: number; y: number };
  useAbsolutePosition: boolean;
}

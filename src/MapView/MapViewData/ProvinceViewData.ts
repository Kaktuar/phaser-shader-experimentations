import Phaser from 'phaser';
import type IMapViewItem from './IMapViewItem';

export default class ProvinceViewData implements IMapViewItem {
  name: string;
  color: string;
  hash: number;
  id: number;
  welthLevel: number;
  absolutePosition: { x: number; y: number };
  useAbsolutePosition = true;
  regionId?: number;

  constructor({
    id,
    name,
    color,
    hash,
    welthLevel,
    absolutePosition,
    useAbsolutePosition,
  }: {
    id: number;
    name: string;
    color: string;
    hash: number;
    welthLevel: number;
    absolutePosition?: { x: number; y: number };
    useAbsolutePosition?: boolean;
  }) {
    this.id = id;
    this.name = name;
    this.color = color;
    this.hash = hash;
    this.welthLevel = welthLevel;
    this.absolutePosition = absolutePosition ?? { x: 0, y: 0 };
    this.useAbsolutePosition = useAbsolutePosition ?? true;
  }

  get getColor(): Phaser.Display.Color {
    return Phaser.Display.Color.HexStringToColor(this.color);
  }

  get getShaderColor(): [number, number, number] {
    const color = Phaser.Display.Color.HexStringToColor(this.color);
    return [color.red / 255.0, color.green / 255.0, color.blue / 255.0];
  }
}

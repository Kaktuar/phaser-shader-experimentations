import Phaser from 'phaser';
import water from '../Sprites/water.png';
import { SetupGlobalKeyboardListener } from '../Tools/KeyboardListener';
import MultiPipelineTest from '../TestPipelines/MultiPipelineTest';

export class TestShaderScene extends Phaser.Scene {
  static readonly key = 'TestShaderScene';
  pipeline: MultiPipelineTest;
  pipelineKey = 'TestShaderSceneShader';

  constructor() {
    super({ key: TestShaderScene.key });
  }

  preload() {
    SetupGlobalKeyboardListener(this);
    this.load.image('Water', water);
  }

  create() {
    const { width } = this.scale;
    const { height } = this.scale;

    const graphics = this.add.graphics();
    graphics.fillStyle(0xffff11, 0);
    graphics.fillRect(0, 0, width, height);
    graphics.generateTexture('blank', width, height);
    const background = this.add.sprite(0, 0, 'blank');
    background.setOrigin(0, 0);
    background.setDisplaySize(width, height);

    this.pipeline = (this.game.renderer as Phaser.Renderer.WebGL.WebGLRenderer).pipelines.get(
      this.pipelineKey
    ) as MultiPipelineTest;
    background.setPipeline(this.pipeline);
    this.pipeline.set1f('green', Math.random());
  }
}

import Phaser from 'phaser';
import { SetupGlobalKeyboardListener } from '../Tools/KeyboardListener';
import MultiPipelineTest from '../TestPipelines/MultiPipelineTest';

export class GlowScene extends Phaser.Scene {
  static readonly key = 'GlowScene';
  pipeline: MultiPipelineTest;
  pipelineKey = 'GlowShader';

  constructor() {
    super({ key: GlowScene.key });
  }

  preload() {
    SetupGlobalKeyboardListener(this);
    if (!this.textures.exists('Goomba')) this.load.image('Goomba', 'src/Sprites/Goomba.png');
  }

  create() {
    this.pipeline = (this.game.renderer as Phaser.Renderer.WebGL.WebGLRenderer).pipelines.get(
      this.pipelineKey,
    ) as MultiPipelineTest;

    const width = this.scale.height;
    const { height } = this.scale;

    const background = this.add.sprite(0, 0, 'Goomba');
    background.setOrigin(0, 0);
    background.setDisplaySize(width, height);

    this.pipeline.set2f('u_resolution', width as number, height as number);
    this.pipeline.set3f('u_glowColor', 1, 1, 0.7);
    this.pipeline.set1f('u_intensity', 1);
    let a = this.textures.get('Goomba').source[0].glTexture as WebGLTexture;
    background.setPipeline(this.pipeline);
    this.pipeline.setTexture2D(a);
    this.pipeline.bindTexture(a);
  }

  update() {
    // Get mouse position
    const mousePosition = this.input.activePointer.position;
    this.pipeline.set2f('mousePos', mousePosition.x, mousePosition.y);
    this.pipeline.set1f('u_time', this.time.now / 100);
  }
}

import MultiPipelineTest from "../TestPipelines/MultiPipelineTest";
import SinglePipelineTest from "../TestPipelines/SinglePipelineTest";
import Goomba from "../Sprites/Goomba.png";
import Water from "../Sprites/water.png";
import { SetupGlobalKeyboardListener } from "../Tools/KeyboardListener";

export class TestShaderPipelineScene extends Phaser.Scene {
  customShader: Phaser.GameObjects.Shader;
  multiPipeline: MultiPipelineTest;
  singlePipeline: SinglePipelineTest;

  static readonly key = "TestShaderPipelineScene";

  constructor() {
    super({ key: TestShaderPipelineScene.key });
  }

  preload() {
    SetupGlobalKeyboardListener(this);
    if (!this.textures.exists("Goomba")) this.load.image("Goomba", Goomba);
    if (!this.textures.exists("Water")) this.load.image("Water", Water);
    this.load.glsl("simplest", "src/MapView/Shaders/TestFX/simplest.frag");
  }

  async create() {
    const sprite1 = this.add.sprite(300, 200, "Goomba");
    const sprite2 = this.add.sprite(100, 300, "Goomba");
    const sprite3 = this.add.sprite(200, 350, "Goomba");
    const sprite4 = this.add.sprite(400, 350, "Goomba");
    sprite1.setScale(0.3, 0.3);
    sprite2.setScale(0.3, 0.3);
    sprite3.setScale(0.3, 0.3);
    sprite4.setScale(0.3, 0.3);

    this.multiPipeline = (this.game.renderer as Phaser.Renderer.WebGL.WebGLRenderer).pipelines.get(
      "MultiPipelineTest"
    ) as MultiPipelineTest;
    this.singlePipeline = (this.game.renderer as Phaser.Renderer.WebGL.WebGLRenderer).pipelines.get(
      "SinglePipelineTest"
    ) as SinglePipelineTest;
    const singlePipelineTexture = (this.game.renderer as Phaser.Renderer.WebGL.WebGLRenderer).pipelines.get(
      "SinglePipelineTexture"
    ) as SinglePipelineTest;
    const multiPipelineTexture = (this.game.renderer as Phaser.Renderer.WebGL.WebGLRenderer).pipelines.get(
      "MultiPipelineTexture"
    ) as MultiPipelineTest;

    singlePipelineTexture.set2f("resolution", this.game.config.width as number, this.game.config.height as number);
    multiPipelineTexture.set2f("resolution", this.game.config.width as number, this.game.config.height as number);

    // Set second texture
    const texture = this.textures.get("Water").source[0].glTexture as WebGLTexture;
    multiPipelineTexture.setTexture2D(texture);
    multiPipelineTexture.bindTexture(texture);
    singlePipelineTexture.setTexture2D(texture);
    singlePipelineTexture.bindTexture(texture); // DOES NOT WORK -> Don't use single pipelines if multiple textures are needed

    this.customShader = this.add.shader("simplest", 0, 0, 200, 100);
    this.customShader.setOrigin(0, 0);
    this.customShader.x = 5;
    this.customShader.y = 5;
    this.customShader.setUniform("green", 0.5); // DOES NOT WORK -> Don't use this way of working with shaders

    sprite1.setPipeline(this.multiPipeline); // or sprite1.setPipeline("MultiPipelineTest");
    sprite2.setPipeline(this.singlePipeline);
    sprite3.setPipeline(singlePipelineTexture);
    /* sprite4.setPipeline(multiPipelineTexture);  
      TODO ->
        After loading this scene : if this multiPipeline is set, some other multiPipelines in other scenes are not working anymore
        -> Understand why
    */
    this.singlePipeline.set1f("green", 0.5);
    this.multiPipeline.set1f("green", 1);
  }

  update(time: number) {
    this.multiPipeline.set1f("green", (time * 0.0003) % 1);
    this.singlePipeline.set1f("green", (time * 0.0003 + 0.3) % 1);
  }
}

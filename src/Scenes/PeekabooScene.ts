import Phaser from 'phaser';
import { SetupGlobalKeyboardListener } from '../Tools/KeyboardListener';
import MultiPipelineTest from '../TestPipelines/MultiPipelineTest';

export class PeekabooScene extends Phaser.Scene {
  static readonly key = 'PeekabooScene';
  pipeline: MultiPipelineTest;
  pipelineKey = 'Peekaboo';

  constructor() {
    super({ key: PeekabooScene.key });
  }

  preload() {
    SetupGlobalKeyboardListener(this);
    if (!this.textures.exists('Goomba')) this.load.image('Goomba', 'src/Sprites/Goomba.png');
  }

  create() {
    this.pipeline = (this.game.renderer as Phaser.Renderer.WebGL.WebGLRenderer).pipelines.get(
      this.pipelineKey,
    ) as MultiPipelineTest;

    const width = this.scale.height;
    const { height } = this.scale;

    const background = this.add.sprite(0, 0, 'Goomba');
    console.log(background);
    background.setOrigin(0, 0);
    background.setDisplaySize(width, height);

    this.pipeline.set2f('resolution', width as number, height as number);
    const texture = this.textures.get('Goomba').source[0].glTexture as WebGLTexture;
    background.setPipeline(this.pipeline);
    this.pipeline.setTexture2D(texture);
    this.pipeline.bindTexture(texture);
  }

  update() {
    // Get mouse position
    const mousePosition = this.input.activePointer.position;
    this.pipeline.set2f('mousePos', mousePosition.x, mousePosition.y);
  }
}

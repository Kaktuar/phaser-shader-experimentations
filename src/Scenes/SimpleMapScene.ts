import Phaser from 'phaser';
import { SetupGlobalKeyboardListener } from '../Tools/KeyboardListener';
import { MapViewCategory, MapView, MapViewSprites } from '../MapView';
import { MockMapDataFetcher } from '../MapView/MapViewData';
import { LoadMapViewPipeline } from '../Tools/ShaderUtilities';

export class SimpleMapScene extends Phaser.Scene {
  static readonly key = 'SimpleMapScene';
  mapView: MapView;

  constructor() {
    super({ key: SimpleMapScene.key });
  }

  preload() {
    SetupGlobalKeyboardListener(this);
    // MapView initialization
    const mapViewSprites: MapViewSprites = {
      map: { key: 'Map', url: 'src/Sprites/map.png' },
      bitmap: { key: 'Bitmap', url: 'src/Sprites/map_colors2.png' },
      mapBorders: { key: 'MapBorders', url: 'src/Sprites/map_borders.png' },
      mapGlow: { key: 'MapGlow', url: 'src/Sprites/map_glow.png' },
    };
    const mapDataFetcher = new MockMapDataFetcher();
    this.mapView = new MapView(this, mapViewSprites, mapDataFetcher);
  }

  create() {
    this.keyboardListenerInitialization();
    (async () => {
      await Promise.all([
        LoadMapViewPipeline(this.game, 'src/MapView/Shaders/Map/mapInternalQuantization.frag', 'MapViewSimple'),
        LoadMapViewPipeline(this.game, 'src/MapView/Shaders/Map/mapNoGlow.frag', 'MapNoGlow'),
        LoadMapViewPipeline(this.game, 'src/MapView/Shaders/Map/mapNoBackgroundCountry.frag', 'MapNoBackground'),
        LoadMapViewPipeline(this.game, 'src/MapView/Shaders/Map/mapExternalQuantization.frag', 'MapViewPipelineTest'),
        LoadMapViewPipeline(this.game, 'src/MapView/Shaders/Map/mapRegion.frag', 'MapRegion'),
        LoadMapViewPipeline(this.game, 'src/MapView/Shaders/Map/mapDebug.frag', 'MapViewDebug'),
        LoadMapViewPipeline(this.game, 'src/MapView/Shaders/Map/mapGlowDebug.frag', 'MapViewGlowDebug'),
      ]);
      // MapView creation
      await this.mapView.create();

      // Mouse handling example
      this.mapView.pointerHandler.onClick(({ x, y }) => {
        console.log(`x:${x}, y:${y}}`);
      });
      this.mapView.pointerHandler.onRegionClick((regionId) => {
        const region = this.mapView.mapViewData.getRegion(regionId);
        console.log(`Region ${regionId}: ${region?.name}`);
      });
      this.mapView.pointerHandler.onProvinceClick((provinceId) => {
        const province = this.mapView.mapViewData.getProvince(provinceId);
        console.log(`Province ${provinceId}: ${province?.name}`);
      });
    })();
  }

  update() {
    this.mapView.update();
  }

  // Debug crap
  isCursorTextInitialized = true;
  private keyboardListenerInitialization() {
    this.input.keyboard?.on('keydown-Q', () => {
      this.mapView.changeRendererPipeline();
    });
    this.input.keyboard?.on('keydown-S', () => {
      this.mapView.toggleDisplayNames((this.isCursorTextInitialized = !this.isCursorTextInitialized));
    });
    // Debug copy bitmap canvas
    const gameDiv = document.getElementById('game');
    // const bitmapImage = this.bitmapTextureHandler.bitmap.getSourceImage();
    const bitmapCanvas = document.createElement('canvas');
    bitmapCanvas.width = this.game.canvas.width;
    bitmapCanvas.height = this.game.canvas.height;
    const bitmapCtx = bitmapCanvas.getContext('2d')!;
    bitmapCtx.drawImage(
      this.textures.get('MapGlow').getSourceImage() as CanvasImageSource,
      0,
      0,
      this.game.canvas.width,
      this.game.canvas.height,
    );
    const copy = gameDiv!.appendChild(bitmapCanvas);
    copy.style.position = 'absolute';
    const gameCanvasPosition = this.game.canvas.getBoundingClientRect();
    copy.style.top = `${gameCanvasPosition.top}px`;
    copy.style.left = `${gameCanvasPosition.left}px`;
    copy.style.display = 'none';
    let toggle = false;
    this.input.keyboard?.on('keydown-F', () => {
      toggle = !toggle;
      this.game.canvas.style.opacity = toggle ? '0' : '1';
      bitmapCanvas.style.display = toggle ? 'block' : 'none';
    });

    this.input.keyboard?.on('keydown-D', () => {
      const mapViewTypes = Object.values(MapViewCategory).filter(
        (value) => typeof value === 'number',
      ) as MapViewCategory[];
      const currentTypeIndex = mapViewTypes.indexOf(this.mapView.mapViewType);
      const newTypeIndex = (currentTypeIndex + 1) % mapViewTypes.length;
      this.mapView.mapViewType = mapViewTypes[newTypeIndex];
    });
  }
}

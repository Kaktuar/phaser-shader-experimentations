import TS from '../typescript.svg';
import { SetupGlobalKeyboardListener } from '../Tools/KeyboardListener';

export class FirstScene extends Phaser.Scene {
  static readonly key = 'MyScene';
  constructor() {
    super({ key: FirstScene.key });
  }

  preload() {
    this.load.image('TS', TS);
  }

  init() {
    SetupGlobalKeyboardListener(this);
  }

  create() {
    this.add.image(200, 100, 'TS');
    this.add.image(30, 10, 'TS');
    this.add.image(150, 30, 'TS');
    this.add.image(150, 150, 'TS');
  }
}

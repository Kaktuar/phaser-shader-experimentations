import { LoadMapViewPipeline, LoadMultiplePipelineTest, LoadSimplePipelineTest } from './Tools/ShaderUtilities';

export async function LoadPipelines(game: Phaser.Game) {
  await Promise.all([
    LoadMultiplePipelineTest(game, 'src/MapView/Shaders/Map/mapInternalQuantization.frag', 'SimpleMap'),
    LoadMultiplePipelineTest(game, 'src/MapView/Shaders/Map/mapExternalQuantization.frag', 'ExternalQuantizationMap'),
    LoadMultiplePipelineTest(game, 'src/MapView/Shaders/Map/mapDebug.frag', 'DebugMap'),
    LoadMultiplePipelineTest(game, 'src/MapView/Shaders/TestFX/peekaboo.frag', 'Peekaboo'),
    LoadMultiplePipelineTest(game, 'src/MapView/Shaders/TestFX/simplest.frag', 'TestShaderSceneShader'),
    LoadMultiplePipelineTest(game, 'src/MapView/Shaders/TestFX/glow.frag', 'GlowShader'),
    LoadMultiplePipelineTest(game, 'src/MapView/Shaders/TestFX/simplest.frag', 'MultiPipelineTest'),
    LoadMultiplePipelineTest(game, 'src/MapView/Shaders/TestFX/multipleTexture.frag', 'MultiPipelineTexture'),
    LoadSimplePipelineTest(game, 'src/MapView/Shaders/TestFX/multipleTexture.frag', 'SinglePipelineTexture'),
    LoadSimplePipelineTest(game, 'src/MapView/Shaders/TestFX/simplest.frag', 'SinglePipelineTest'),
  ]);
}

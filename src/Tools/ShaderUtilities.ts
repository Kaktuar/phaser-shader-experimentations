import { MapViewPipeline } from '../MapView';
import MultiPipelineTest from '../TestPipelines/MultiPipelineTest';
import SinglePipelineTest from '../TestPipelines/SinglePipelineTest';

export async function loadShader(url: string) {
  const response = await fetch(url);
  const text = await response.text();
  return text;
}

export async function LoadMultiplePipelineTest(game: Phaser.Game, fragShaderUrl: string, key: string) {
  const fragShader = await loadShader(fragShaderUrl);

  const pipeline = new MultiPipelineTest(game, fragShader, key);
  (game.renderer as Phaser.Renderer.WebGL.WebGLRenderer).pipelines.add(pipeline.key, pipeline);

  return pipeline;
}

export async function LoadMapViewPipeline(game: Phaser.Game, fragShaderUrl: string, key: string) {
  const fragShader = await loadShader(fragShaderUrl);

  const pipeline = new MapViewPipeline(game, fragShader, key);
  (game.renderer as Phaser.Renderer.WebGL.WebGLRenderer).pipelines.add(pipeline.key, pipeline);

  return pipeline;
}

export async function LoadSimplePipelineTest(game: Phaser.Game, fragShaderUrl: string, key: string) {
  const fragShader = await loadShader(fragShaderUrl);

  const pipeline = new SinglePipelineTest(game, fragShader, key);
  (game.renderer as Phaser.Renderer.WebGL.WebGLRenderer).pipelines.add(pipeline.key, pipeline);
}

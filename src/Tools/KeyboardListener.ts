import * as Scenes from '../Scenes';

export function SetupGlobalKeyboardListener(scene: Phaser.Scene) {
  let sceneKeys = Object.values(Scenes).map((scene) => scene.key);
  sceneKeys.forEach((key, index) => {
    scene.input.keyboard?.on(keyDownEvents[index], () => {
      scene.scene.switch(key);
      // check if scene is SimpleMapScene
      if (scene instanceof Scenes.SimpleMapScene) {
        const a = scene as Scenes.SimpleMapScene;
        a.mapView.refreshRendering();
      }
    });
  });
}

let keyDownEvents: Array<string> = [
  'keydown-A',
  'keydown-Z',
  'keydown-E',
  'keydown-R',
  'keydown-T',
  'keydown-Y',
  'keydown-U',
  'keydown-I',
];

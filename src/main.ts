import "./style.css";
import Phaser from "phaser";
import * as Scenes from "./Scenes";
import { LoadPipelines } from "./PipelineLoader";

const scenes = Object.values(Scenes);

console.log(scenes);

const config = {
  type: Phaser.AUTO,
  width: 512,
  height: 512,
  backgroundColor: "#2d2d2d",
  parent: "game",
  scene: scenes,
  // callbacks: {
  //   postBoot (game: Phaser.Game) {},
  // },
};

const game = new Phaser.Game(config);
await LoadPipelines(game);
const currentScene = game.scene.getScene(scenes[0].key);
currentScene.scene.stop();
game.scene.start(scenes[3].key);
